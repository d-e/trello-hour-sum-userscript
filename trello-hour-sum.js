// ==UserScript==
// @name        Trello time add
// @namespace   gr.d-e
// @description Adds the total time to boards
// @include     https://trello.com/b/qoadBj3W/scrum
// @version     1
// @grant       none
// ==/UserScript==

$(window).load(function(){
  $('#header').click(function(){
    $('.list').each(function(){
      var totalhours = 0
      $(this).find('.list-card-title').each(function(){
        var match = $(this).text().match(/\(([\d]+)h\)/)
        if (match != null) {
          var hours = parseInt(match[1])
          console.log($(this).text()+" "+hours);
          totalhours += hours;
        }
      })
      //console.log(totalhours);
      var old = $(this).find('.list-header').html()
      var title = $(this).find('.list-header')
      if (title.children('span').length == 0)
        title.append("<span>"+totalhours+"h</span>")
      else title.children('span').html(totalhours+"h");
    })
  })
  //$('.list-wrapper:eq(1)').after('<div style=width:5px;height:100%;background:red;display:inline-block ></div>')
  $('.list:lt(2)').css('background','#bce')
})

