# A userscript to add hours in every list in trello

This script simply finds all occurrences of (XXh) where XX is a number in every
card and adds them up. The total of every list is displayed next to the list 
name. To trigger a calculation you have to click on the empty space of the top toolbar, next to the trello logo.

For the script to work you will need to refresh the page when in the board view.
If you go back and forth to the trello main page you will need to refresh again.

Additionally it changes the background color of the first two lists, in order
to make the distinction between **backlog items** and **tasks** in SCRUM, when 
trello is used as described [here](https://medium.com/p/915308ef75b8/)

You will need [greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/) and [firefox](http://firefox.com) or chrome and a greasemonkey equivalent. 
